# Garden State gs-sensors-proto
Protocol buffer implemented in Go to serialize measures data.
[Protobuf](https://developers.google.com/protocol-buffers/docs/gotutorial)

### Compile proto file
```
$ go get -u google.golang.org/grpc
$ go get -u github.com/golang/protobuf/{proto,protoc-gen-go}
$ protoc measures.proto --go_out=plugins=grpc:.
```
